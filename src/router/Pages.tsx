import { useRoutes } from 'react-router-dom';
import ImagePage from '../modules/captcha-images';
/**
 * 页面容器组件
 */
export default function PageContainer() {
  let routes = useRoutes([
    { path: "", element: <ImagePage /> }
  ]);
  return routes;
}