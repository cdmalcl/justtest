import styled from 'styled-components';
import './App.css';
import PageContainer from './router/Pages';
import { BrowserRouter } from 'react-router-dom';

const AppWrap = styled.div`
  width: 100%;
`;

function App() {
  return (
    <AppWrap>
      <BrowserRouter>
        <PageContainer />
      </BrowserRouter>
    </AppWrap>
  );
}

export default App;
