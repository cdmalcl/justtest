import { createContext } from "react";

export interface ImageFileItem {
  fileName: string;
  filePath: string;
  imgBase64: string;
  captcha: string;
  words: string[];
}

export interface SelWordItem {
  item: ImageFileItem;
  wordIndex: number;
}

export interface ImageContextInterface {
  curSel: SelWordItem | null;
  setCurSel: (item: SelWordItem | null) => void;
}

const defValue = {
  curSel: null,
  setCurSel: (item: SelWordItem | null) => {},
};

const ImageContext = createContext<ImageContextInterface>(defValue);

export default ImageContext;