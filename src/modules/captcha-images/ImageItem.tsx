import { message } from 'antd';
import { useContext } from 'react';
import styled from 'styled-components';
import { api } from '../../utils/request';
import ImageContext, { ImageFileItem } from './ImageContext';
import WordOne from './WordOne';

const ImageItemWrap = styled.div`
  width: 450px;
  height: 100px;
  display: flex;
  flex-direction: row;
  margin: 20px 10px;
  box-shadow: 2px 2px 5px rgba(48, 48, 48, 0.3);
`;

const Left = styled.div`
  width: 180px;
  height: 90px;
  display: flex;
`;

const Right = styled.div`
  width: 260px;
  height: 90px;
  display: flex;
  margin-left: 10px;
  display: flex;
  flex-direction: column;
`;

const CaptchaImageWrap = styled.div`
  width: 180px;
  height: 90px;
  padding: 10px;
`;

const CaptchaImage = styled.img`
  width: 180px;
  height: 90px;
  display: block;
`;

const CaptchaWords = styled.div`
  width: 260px;
  height: 60px;
  display: flex;
  flex-direction: row;
`;

interface Props { 
  item: ImageFileItem; 
  onWordsChange: (captcha: string) => void;
  onMoveRight: (item: ImageFileItem) => void;
}

export default function ImageItem(props: Props) {
  const {
    item,
    onWordsChange,
    onMoveRight
  } = props;
  
  let isSel = false;
  let selIndex = -1;
  const imageContext = useContext(ImageContext);
  if (imageContext.curSel && imageContext.curSel?.item.fileName === item.fileName) {
    isSel = true;
    selIndex = imageContext.curSel.wordIndex;
  }

  const onWordChnage = (index: number, word: string) => {
    let words = item.words;
    words[index] = word;
    onWordsChange(words.join(''))
    console.log(index, word, words)
  }

  const onSelWord = (index: number) => {
    if (isSel && imageContext.curSel?.wordIndex === index) {
      imageContext.setCurSel(null);
      return;
    }
    imageContext.setCurSel({
      item,
      wordIndex: index
    })
  }
  
  const onDblClick = () => {
    onMoveRight(item);
  }
  return (
    <ImageItemWrap>
      <Left>
        <CaptchaImageWrap>
          <CaptchaImage src={item.imgBase64} onDoubleClick={onDblClick}/>
        </CaptchaImageWrap>
      </Left>
      <Right>
        <CaptchaWords>
          {item.words.map((word: string, index: number) => {
            const isSelWord = isSel && selIndex === index;
            return <WordOne 
              key={index} 
              word={word} 
              isSel={isSelWord} 
              onSelWord={() => onSelWord(index)}
              onChange={(word) => onWordChnage(index, word)} />
          })}
        </CaptchaWords>
      </Right>
    </ImageItemWrap>
  );
}