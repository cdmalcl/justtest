import styled from 'styled-components';
import { ImageFileItem } from './ImageContext';
import ImageItem from "./ImageItem";

const ImageListWrap = styled.div`
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

interface Props {
  list: Array<ImageFileItem>;
  onItemCaptchaChange: (index: number, captcha: string) => void;
  onMoveRight: (item: ImageFileItem) => void;
}

export default function ImageListEdit(props: Props) {
  const {
    list,
    onItemCaptchaChange,
    onMoveRight
  } = props;
  const onWordsChange = (index: number, captcha: string) => {
    onItemCaptchaChange(index, captcha)
  }
  return (
    <ImageListWrap>
      {list.map((item, index) => {
        return <ImageItem key={item.fileName} item={item} onWordsChange={(captcha) => onWordsChange(index, captcha)} onMoveRight={onMoveRight}/>
      })}
    </ImageListWrap>
  );
}