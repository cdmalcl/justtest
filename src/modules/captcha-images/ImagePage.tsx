import { useState } from "react";
import ImageListEdit from "./ImageList";
import { Input, message, Pagination } from 'antd';
import Hotkeys from 'react-hot-keys';
import styled from "styled-components";
import { api } from "../../utils/request";
import ImageContext, { ImageFileItem, SelWordItem } from "./ImageContext";

const { Search } = Input;

const Container = styled.div`
  width: 100%;
  text-align: center;
`;

const ImgPathInput = styled(Search)`
  max-width: 500px;
  margin-top: 50px;
`;

export default function ImagePage() {
  const [list, setList] = useState(Array<ImageFileItem>());
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [imgPathInput, setImgPathInput] = useState('D:\\projects\\cnn_captcha\\sample\\bk\\');
  const [isLoading, setIsLoading] = useState(false);
  const [curSel, setCurSel] = useState<SelWordItem | null>(null);
  
  const providerValue = {
    curSel,
    setCurSel
  };

  const handleKeyDown = (keyName: string, e: any) => {
    if (!curSel) {
      return;
    }
    if (!/^[a-z|A-Z|0-9]{1}$/.test(e.key)) {
      return;
    }
    if (curSel.item.words[curSel.wordIndex] === e.key) {
      return;
    }
    let words = curSel.item.words
    words[curSel.wordIndex] = e.key;
    doChangePath(curSel.item, words.join(''));
  }

  const loadList = (imgPath: any, page: number) => {
    setIsLoading(true)
    api.get('imglist', {
      params: {
        page,
        imgdir: imgPath
      }
    }).then((res: any) => {
      console.log("res", res);
      setList(res.data.data);
      setCount(res.data.count);
      setIsLoading(false)
    }).catch((err: any) => {
      console.log(err);
      setIsLoading(false)
    })
  }  
  const onPageChange = (curPage: number, pageSize: number) => {
    setPage(curPage);
    loadList(imgPathInput, curPage)
  }
  const onSearch = (value: string) => {
    setImgPathInput(value)
    loadList(value, page);
  }
  
  const doChangePath = (item: ImageFileItem, captcha: string) => {
    const arr = item.fileName.split('_');
    if (arr.length !== 2) {
      return;
    }
    const newPath = item.filePath.substr(0, item.filePath.length - item.fileName.length) + captcha + '_' + arr[1];
    setIsLoading(true)
    api.get('changePath', {
      params: {
        newPath: newPath,
        oldPath: item.filePath
      }
    }).then((res: any) => {
      console.log("res", res);
      message.success('保存成功');
      setIsLoading(false);
      loadList(imgPathInput, page)
    }).catch((err: any) => {
      console.log(err);
      setIsLoading(false);
    })
  }
  const onItemCaptchaChange = (index: number, captcha: string) => {
    const item = list[index]
    doChangePath(item, captcha);
  }
  
  const onMoveRight = (item: ImageFileItem) => {
    console.log(item.filePath)
    api.get('moveRight', {
      params: {
        path: item.filePath
      }
    }).then((res: any) => {
      message.success('保存成功');
      loadList(imgPathInput, page)
    }).catch((err: any) => {
    })
  }

  return (
    <ImageContext.Provider value={providerValue}>
      <Hotkeys 
        keyName="*"
        onKeyUp={handleKeyDown}
      >
      <Container>
        <ImgPathInput enterButton="查询" size="large" loading={isLoading} onSearch={onSearch} defaultValue={imgPathInput}/>
        <ImageListEdit list={list} onItemCaptchaChange={onItemCaptchaChange} onMoveRight={onMoveRight}/>
        {
          count > 0 && <Pagination showQuickJumper defaultCurrent={page} defaultPageSize={20} total={count} onChange={onPageChange} />
        }        
      </Container>
      </Hotkeys>
    </ImageContext.Provider>
  )
}