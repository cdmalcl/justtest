import styled from 'styled-components';

interface Props {
  word: string,
  onChange: (word: string) => void;
  isSel: boolean;
  onSelWord: () => void;
}

interface WordProps {
  isSel: boolean;
}

const WordOneWrap = styled.div`
  width: 65px;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Word = styled.div`
  width: 50px;
  height: 50px;
  font-size: 30px;
  font-weight: bold;
  border: 1px solid #f0f0f0;
  cursor: pointer;
  background: ${(props: WordProps) => (props.isSel ? '#1890ff': '#fff')};
  color: ${(props: WordProps) => (props.isSel ? '#fff': '#333')}
`;

export default function WordOne(props: Props) {
  const { 
    word,
    onChange,
    isSel,
    onSelWord
  } = props;
  const onDblClick = () => {
    if (/[a-z]/.test(word)) {
      onChange(word.toUpperCase())
    } else if (/[A-Z]/.test(word)) {
      onChange(word.toLowerCase())
    }
  }

  const onSel = () => {
    onSelWord()
  }
  return (
    <WordOneWrap>
      <Word onDoubleClick={onDblClick} onClick={onSel} isSel={isSel}>{word}</Word>
    </WordOneWrap>
  );
}