import axios from "axios";

export default axios;

const api = axios.create({
    baseURL: "http://127.0.0.1:8987/api/",
    timeout: 10000  //请求超时时间
})

api.interceptors.response.use(function (response) {
  if (typeof response.data.code === 'undefined' || response.data.code > 0) {
    return Promise.reject(response.data);
  } else {
    return Promise.resolve(response.data);
  }
}, function (error) {
  return Promise.reject(error);
});

export {
  api
}